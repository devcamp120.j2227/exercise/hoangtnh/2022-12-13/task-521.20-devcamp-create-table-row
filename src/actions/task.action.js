import { TASK_ADD_ROW_CLICK, TASK_INPUT_CHANGE } from "../constants/task.constants";

const inputChangeHandler = (value) =>{
    return{
        type: TASK_INPUT_CHANGE,
        payload: value
    }
}

const buttonAddRow = () =>{
    return {
        type: TASK_ADD_ROW_CLICK
    }
}


export {
    inputChangeHandler,
    buttonAddRow
}